# Please read

## Structures 

* 4 initial folders with names and stud IDs: put your individual assignments in here, and only modify folder which has your name on it.
* The merge and final build file will be in the T-Rex Build folder. For now, you can upload your developing pages in individual folder and merge in the build folder later.
---
## Set Up.

There are multiple ways you can works with Git, you can use either SourceTree, Git Terminal, VS Code built-in version controls and extensions or other tools that you like.

* [SourceTree](https://support.atlassian.com/bitbucket-cloud/docs/tutorial-learn-bitbucket-with-sourcetree/)
* [Git Bash](https://www.atlassian.com/git/tutorials/git-bash)
* [Bitbucket in VS Code](https://support.atlassian.com/bitbucket-cloud/docs/get-started-with-vs-code/).

In general, you need to clone this repo into your local machine for the 1st time, edit some, commit and push to this repo so it is updated with your code. For subsequent adjustments, pull instead of clone to update your local repo with changes in remote repo.

### On Git Pull 
Everytime someone make a commit and push to bitbucket repo, you have to update your local repo with latest changes from the remote repo before pushing your own changes. Please read this [one](https://stackoverflow.com/questions/19085807/please-enter-a-commit-message-to-explain-why-this-merge-is-necessary-especially) and follow the second answer if you use git bash for pulling.

---

## Note
Please follow the style in the existing pages for a consistent styling across pages. This includes color, font and elements such as buttons, cards. Also, try to structure your code and file in a systematic way so others can understand when reading your code.

---
## Project File Structure
This is the recommended file structures for the project.
TBA

---
## Host Static Site on Bitbucket
[How to host a site on Bitbucket Cloud](https://support.atlassian.com/bitbucket-cloud/docs/publishing-a-website-on-bitbucket-cloud/)