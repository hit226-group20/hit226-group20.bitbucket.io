import { createAndSaveNewUser } from "./lib/data.js";
import { searchFunc } from "./search.js";
import {
  openAndCloseMenu,
  showSearchBar,
  onResizeWindow,
  toggleLoginModal,
  submitLoginForm,
  displayLoginSection,
  logOut,
} from "./navbar.js";

$(document).ready(function () {
  $(".navbar").load("./pages/navbar.html nav", () => {
    displayLoginSection(sessionStorage.getItem("isLoggedIn"));
  });
  $(".footer").load("./pages/footer.html footer");

  /* Navbar */
  openAndCloseMenu();
  showSearchBar();
  onResizeWindow();
  toggleLoginModal();
  submitLoginForm();
  logOut();

  /*Search*/
  searchFunc();

  // Check existing user and create one if new.
  $(".create-user").on("submit", (e) => {
    e.preventDefault();
    let username = $("#newUsername").val();
    let password = $("#newPassword").val();
    
    if(createAndSaveNewUser(username, password)) {
      sessionStorage.setItem("isLoggedIn", true);
      sessionStorage.setItem("usernameLoggingIn", username);
      displayLoginSection(sessionStorage.getItem("isLoggedIn"));
      window.location.href = "./index.html"
    };
  });
});
