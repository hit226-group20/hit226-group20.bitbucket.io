import { data } from "./lib/data.js";

/**
 * Desc: Sort data alphabetically according to names
 * In: raw imported data;
 * Out: Array of sorted data
 */
const sortData = (data) => {
  return data.sort((a, b) => (a.name > b.name ? 1 : -1));
};

/**
 * Desc: Search games based on search query
 * In: search query on search bar
 * Out: Array of object results.
 */
const searchAlgo = (query) => {
  const reg = new RegExp(query, "i");
  let dataSorted = sortData(data);
  return dataSorted
    .filter((game) => reg.test(game.name))
    .map((game) => {
      return {
        name: game.name,
        id: game.id,
      };
    });
};

/** 
 * Desc: Create list tag based on search results
 * In: Array of object results.
 * Out: Array of list tags.
 */
const generateDropDownList = (searchResults) => {
  return searchResults.map((result) => {
    return `<li class="result"><a href="./singleGame.html?game=${result.id}">${result.name}</a></li>`;
  });
};

/** 
 * Desc: Generate html elements of search results to add to drop down lists
 * In: Array of list tags.
 * Out: Add list tag to ul element.
 */
const generateDropDown = (query) => {
  let lists = "";
  generateDropDownList(searchAlgo(query)).forEach((result) => {
    if (result) {
      lists += result;
    }
  });
  if (lists == "") lists = "<li>No games found.</li>";
  $(".searchResults-container").html(lists).css("display", "flex");
};

/**  
 * Desc: Capture users input in search bar and passed it to generate function
 */
const searchFunc = () => {
  let query;
  let timerID;
  //Large screen searchbar event when submenu is hidden.
  $(".navbar").on("keyup", "#search-query-on-desktop", function () {
    query = $(this).val();
    if (!timerID) {
      timerID = setTimeout(() => {
        generateDropDown(query);
      }, 1000);
    } else {
      //Clear and reset timer
      clearTimeout(timerID);
      timerID = setTimeout(() => {
        generateDropDown(query);
      }, 1000);
    }
  });
  //Small Screen searchbar event when submenu is showed.
  $(".navbar").on("keyup", "#search-query", function () {
    query = $(this).val();
    if (!timerID) {
      timerID = setTimeout(() => {
        generateDropDown(query);
      }, 1000);
    } else {
      //Clear and reset timer
      clearTimeout(timerID);
      timerID = setTimeout(() => {
        generateDropDown(query);
      }, 1000);
    }
  });
};

const hideSearchContainer = () => {
  $(".searchResults-container").css("display", "none");
}
export { searchFunc, hideSearchContainer };
