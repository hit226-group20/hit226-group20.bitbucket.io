import {createAndSaveNewUser} from './lib/data.js';

$(document).ready(function () {
  $('.create-user').on('submit',(e) => {
    e.preventDefault();
    let username = $('#username').val();
    let password = $('#password').val();
    createAndSaveNewUser(username, password);
  });

})

// $('.check-user').on('submit',(e) => {
//   e.preventDefault();
//   let username = $("#name").val();
//   let password = $("#pw").val();
//   //checkUser(username, password);
// })