/**
 * TODO:
 * Login/Register Popup/Page.
 */

import {
  openAndCloseMenu,
  showSearchBar,
  onResizeWindow,
  toggleLoginModal,
  submitLoginForm,
  displayLoginSection,
  logOut
} from "./navbar.js";

import { setPosition, controlSlide, stopAndPlaySlide } from "./heroSlider.js";
import { searchFunc } from "./search.js";

$(document).ready(function () {
  //Link relative to the html file not JS file
  $(".navbar").load("./pages/navbar.html nav", () => {
    displayLoginSection(sessionStorage.getItem("isLoggedIn"));
  });
  $(".footer").load("./pages/footer.html footer");


  /* Navbar */
  openAndCloseMenu();
  showSearchBar();
  onResizeWindow();
  toggleLoginModal();
  submitLoginForm();
  logOut();

  /*Search*/
  searchFunc();

  /*Search*/
  searchFunc();

  /* Hero Slider*/
  setPosition();
  stopAndPlaySlide();
  controlSlide();

  /* Card Component */
  $(".submitAnchorTag").on("click", function () {
    $(this).closest("form").submit();
  });

  // Hide play button by default.
  $(".fa-play").hide();

  //Change Category colors
  [...$(".category")].forEach((ele) => {
    ele.classList.add(`${ele.innerText}`);
  });
});
