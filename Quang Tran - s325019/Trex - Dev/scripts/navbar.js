import { checkUser } from "./lib/data.js";
import { hideSearchContainer } from "./search.js";

let isMenuOpen = false;
let isSearchInputOpen = false;

/**
 * Desc: Open and Close Sub-menu on Small screen
 */
const openAndCloseMenu = () => {
  $(".navbar").on("click", ".fa-bars", () => {
    //console.log("open");
    if (!isMenuOpen) {
      $(".sub-menu").css({
        transform: "translateY(0)",
        opacity: 1,
        pointerEvents: "unset",
      });
      isMenuOpen = true;
      $("body").addClass("menuOpen");
    } else {
      $(".sub-menu").css({
        transform: "translateY(-200%)",
        opacity: 0,
        pointerEvents: "none",
      });
      isMenuOpen = false;
      isSearchInputOpen = false;
      $("#search-query").hide();
      hideSearchContainer();
      $("body").removeClass("menuOpen");
    }
  });
};

/**
 * Desc: Animation for search bar
 */
const searchBarAnim = () => {
  let tl = gsap.timeline();
  tl.fromTo(
    "#hide-on-search-click",
    {
      x: "0",
      opacity: 1,
      pointerEvents: "initial",
    },
    {
      x: "+60px",
      opacity: 0,
      onComplete: function () {
        this.targets()[0].style.pointerEvents = "none";
      },
    }
  ).fromTo(
    "#search-query-on-desktop",
    {
      x: "0",
      opacity: 0,
      pointerEvents: "none",
    },
    {
      x: "-250px",
      opacity: 1,
      //Enable typing after animation
      onComplete: function () {
        this.targets()[0].style.pointerEvents = "initial";
      },
    },
    "+=.2"
  );
  return tl;
};

/**
 * Desc: add event listeners for showing and hiding search bar input.
 */
const showSearchBar = () => {
  $(".navbar").on("click", ".fa-search", () => {
    if (!isSearchInputOpen) {
      if (window.innerWidth < 1250) {
        $("#search-query").show().focus();
      } else {
        searchBarAnim();
      }
      isSearchInputOpen = true;
    } else {
      if (window.innerWidth < 1250) {
        $("#search-query").hide();
      } else {
        //Reverse the animation from the end, 0 parameter is needed.
        searchBarAnim().reverse(0);
      }
      hideSearchContainer();
      isSearchInputOpen = false;
    }
  });
};

/**
 * Desc: add 2 event listeners for showing and closing login modal.
 */
const toggleLoginModal = () => {
  $(".navbar").on("click", ".login", () => {
    if (isMenuOpen) {
      // On small screen
      //Close submenu
      $(".sub-menu").css("transform", "translateY(-200%)").css("opacity", "0");
      isMenuOpen = false;
      isSearchInputOpen = false;
      $("#search-query").hide();

      //Show login modal
      $(".login-modal-container").css({
        transform: "translateY(0)",
        opacity: 1,
      });
    } else {
      // On big screen
      $(".login-modal-container").css({
        transform: "translateY(0)",
        opacity: 1,
      });
      hideSearchContainer();
      $("body").addClass("menuOpen");
    }

    $(".navbar").on("click", ".exit-modal-btn", () => {
      $(".login-modal-container").css({
        transform: "translateY(-200%)",
        opacity: 0,
      });
      if ($("body").hasClass("menuOpen")) {
        $("body").removeClass("menuOpen");
      }
    });
  });
};

/**
 * Desc: Change login section display based on log in status of users.
 * In: log in status.
 * Out: Log in section display.
 */

const displayLoginSection = (isLoggedIn) => {
  displayLoginSectionOnMobile(isLoggedIn);
  displayLoginSectionOnDesktop(isLoggedIn);
};

const displayLoginSectionOnMobile = (isLoggedIn) => {
  let html = "";
  if (isLoggedIn == "true") {
    let username = sessionStorage.getItem("usernameLoggingIn");
    html = `<li class="show-when-logged-in">
                <p class="logged-in-mes">Hello ${username}.</p>
                <button class="log-out-btn personal-btn">Log Out.</button>
              </li>
              `;
  } else {
    html = `<li>
              <a href="#" class="personal-btn login">Login</a>
            </li>
            <li>
              <a href="./register.html" id="register" class="personal-btn cta-btn"
                >Register</a
              >
            </li>`;
  }
  html += `<li>
              <i class="fas fa-search"></i>
              <input
                type="text"
                name="search-query"
                id="search-query"
                placeholder="Find your favorite games"
              />
              <ul class="searchResults-container"></ul>
            </li>`;
  $(".sub-menu .personal-menu").html(html);
};

const displayLoginSectionOnDesktop = (isLoggedIn) => {
  let html = `<li>
  <div id="hide-on-search-click">Search</div>
  <input
  type="text"
  name="search-query"
  id="search-query-on-desktop"
  placeholder="Find your favorite games"
  />
  <ul class="searchResults-container"></ul>
  <div class="search-btn">
  <i class="fas fa-search"></i>
  </div>
  </li>`;
  if (isLoggedIn == "true") {
    let username = sessionStorage.getItem("usernameLoggingIn");
    html += `<li class="show-when-logged-in">
    <p class="logged-in-mes">Hello ${username}.</p>
    <button class="log-out-btn personal-btn">Log Out.</button>
    </li>`;
  } else {
    html += `<li>
            <a href="#" class="personal-btn login">Login</a>
          </li>
          <li>
            <a href="./register.html" id="register" class="personal-btn cta-btn">Register</a>
          </li>`;
  }
  $("#on-desktop").html(html);
};

/**
 * Desc: Check users credentials.
 */
const submitLoginForm = () => {
  //Uses onsubmit not onclick to retain form validation
  $(".navbar").on("submit", ".login-form", (e) => {
    e.preventDefault();
    let username = $("#username").val();
    let password = $("#password").val();
    /**
     * If checkuser => show login section with face and info.
     * else have notification
     */
    if (checkUser(username, password)) {
      $(".login-modal-container").css({
        transform: "translateY(-200%)",
        opacity: 0,
      });
      $(".sub-menu").css({
        transform: "translateY(0)",
        opacity: 1,
      });
      isMenuOpen = true;
      if (window.innerWidth >= 1250) {
        $("body").removeClass("menuOpen");
      }

      sessionStorage.setItem("isLoggedIn", true);
      sessionStorage.setItem("usernameLoggingIn", username);
      displayLoginSection(sessionStorage.getItem("isLoggedIn"));
    } else {
      alert("Wrong username or password, please type it again.");
    }
  });
};

/**
 * Desc: When users log out, they will be redirected to homepage.
 */
const logOut = () => {
  $(".navbar").on("click", (e) => {
    if ($(e.target).hasClass("log-out-btn")) {
      sessionStorage.setItem("isLoggedIn", false);
      window.location.href = "./index.html";
    }
  });
};

/**
 * Desc: Listen for resize event and change body class
 */
const onResizeWindow = () => {
  $(window).on("resize", () => {
    $(".searchResults-container").hide();
    if (window.innerWidth >= 1250) {
      isMenuOpen = false;
      isSearchInputOpen = false;
      $("body").removeClass("menuOpen");
    } else if (window.innerWidth < 1250 && isMenuOpen == false) {
      $(".sub-menu").css("transform", "translateY(-200%)").css("opacity", "0");
      $("#search-query").hide();
    }
  });
};

export {
  openAndCloseMenu,
  showSearchBar,
  onResizeWindow,
  toggleLoginModal,
  submitLoginForm,
  displayLoginSection,
  logOut,
};
