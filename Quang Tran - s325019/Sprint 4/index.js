const buttons = document.querySelectorAll(".fas");
const hiddenSection = document.querySelectorAll(".hidden");
const redirectBtn = document.querySelectorAll(".action-btn");
// Make sure to refresh classes on load
document.addEventListener("DOMContentLoaded", () => {
  hiddenSection[0].classList.remove("show");
  hiddenSection[0].classList.remove("hide");
  hiddenSection[1].classList.remove("show");
  hiddenSection[1].classList.remove("hide");
  // Read more Buttons
  for (btn of redirectBtn) {
    btn.addEventListener("click", function () {
      if (this.classList.contains("content")) {
        window.location =
          "https://hit226-group20.bitbucket.io/Quang%20Tran%20-%20s325019/Week%200/#content-strategist";
      } else {
        window.location =
          "https://hit226-group20.bitbucket.io/Quang%20Tran%20-%20s325019/Week%200/#visual-designer";
      }
    });
  }
});
// Add event listener on button click
for (btn of buttons) {
  btn.addEventListener("click", function () {
    this.classList.toggle("fa-rotate-180");
    if (this.classList.contains("content")) {
      if (hiddenSection[0].classList.contains("show")) {
        hiddenSection[0].classList.remove("show");
        hiddenSection[0].classList.add("hide");
      } else {
        hiddenSection[0].classList.remove("hide");
        hiddenSection[0].classList.add("show");
      }
    } else {
      if (hiddenSection[1].classList.contains("show")) {
        hiddenSection[1].classList.remove("show");
        hiddenSection[1].classList.add("hide");
      } else {
        hiddenSection[1].classList.remove("hide");
        hiddenSection[1].classList.add("show");
      }
    }
  });
}
