let isMenuOpen = false;
let isSearchInputOpen = false;

//Toggle menu open and close.
const openAndCloseMenu = () => {
  $(".navbar").on("click", ".fa-bars", () => {
    //console.log("open");
    if (!isMenuOpen) {
      $(".sub-menu").css("transform", "translateY(0)").css("opacity", "1");
      isMenuOpen = true;
      $("body").addClass("menuOpen");
    } else {
      $(".sub-menu").css("transform", "translateY(-200%)").css("opacity", "0");
      isMenuOpen = false;
      isSearchInputOpen = false;
      $("#search-query").hide();
      $("body").removeClass("menuOpen");
    }
  });
};

//Animation on searchbar on desktop
const anim = () => {
  let tl = gsap.timeline();
  tl.fromTo(
    "#hide-on-search-click",
    {
      x: "0",
      opacity: 1,
      pointerEvents: "initial",
    },
    {
      x: "+60px",
      opacity: 0,
      onComplete: function () {
        this.targets()[0].style.pointerEvents = "none";
      },
    }
  ).fromTo(
    "#search-query-on-desktop",
    {
      x: "0",
      opacity: 0,
      pointerEvents: "none",
    },
    {
      x: "-250px",
      opacity: 1,
      //Enable typing after animation
      onComplete: function () {
        this.targets()[0].style.pointerEvents = "initial";
      },
    },
    "+=.2"
  );
  return tl;
};

const searchBarAnim = () => {
  $(".navbar").on("click", ".fa-search", () => {
    if (!isSearchInputOpen) {
      if (window.innerWidth < 1250) {
        $("#search-query").show().focus();
        console.log("test");
      } else {
        anim();
      }
      isSearchInputOpen = true;
    } else {
      if (window.innerWidth < 1250) {
        $("#search-query").hide();
      } else {
        //Reverse the animation from the end, 0 parameter is needed.
        anim().reverse(0);
      }
      isSearchInputOpen = false;
    }
  });
};

//Listen for resize event and change body class
const onResizeWindow = () => {
  $(window).on("resize", () => {
    if (window.innerWidth >= 1250) {
      isMenuOpen = false;
      isSearchInputOpen = false;
      $("body").removeClass("menuOpen");
    } else if (window.innerWidth < 1250 && isMenuOpen == false) {
      $(".sub-menu").css("transform", "translateY(-200%)").css("opacity", "0");
      $("#search-query").hide();
    }
  });
};

export { openAndCloseMenu, searchBarAnim, onResizeWindow };
