import { openAndCloseMenu, searchBarAnim, onResizeWindow } from "./navbar.js";
import { setPosition, controlSlide, stopAndPlaySlide } from "./heroSlider.js";

$(document).ready(function () {
  //Link relative to the html file not JS file
  $(".navbar").load("./pages/navbar.html");
  $(".footer").load("./pages/footer.html");
  //Global State

  /* Navbar */
  openAndCloseMenu();
  searchBarAnim();
  onResizeWindow();

  /* Hero Slider*/
  setPosition();
  stopAndPlaySlide();
  controlSlide();

  // Hide play button by default.
  $(".fa-play").hide();

  //Change Category colors
  [...$(".category")].forEach((ele) => {
    ele.classList.add(`${ele.innerText}`);
  });
});
