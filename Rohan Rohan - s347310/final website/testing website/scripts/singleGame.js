import { openAndCloseMenu, searchBarAnim, onResizeWindow } from "./navbar.js";

$(document).ready(function () {
  //Link relative to the html file not JS file
  $(".navbar").load("./pages/navbar.html");
  $(".footer").load("./pages/footer.html");

  /* Navbar */
  openAndCloseMenu();
  searchBarAnim();
  onResizeWindow();

  [...$(".category")].forEach((ele) => {
    ele.classList.add(`${ele.innerText}`);
  });
});
