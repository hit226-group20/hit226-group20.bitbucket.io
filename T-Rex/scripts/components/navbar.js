/**
 * This scripts contains all navigation bar functionalities.
 */
import { hideSearchContainer } from "./search.js";

let isSearchInputOpen = false;

/**
 * Desc: Open and Close Sub-menu on Small screen
 */
const openAndCloseMenu = () => {
  $(".navbar").on("click", ".fa-bars", () => {
    //console.log("open");
    if ($(".sub-menu").css("opacity") == 0) {
      $(".sub-menu").css({
        transform: "translateY(0)",
        opacity: 1,
        pointerEvents: "unset",
      });
      $("body").addClass("menuOpen");
    } else {
      $(".sub-menu").css({
        transform: "translateY(-200%)",
        opacity: 0,
        pointerEvents: "none",
      });
      isSearchInputOpen = false;
      $("#search-query").hide();
      hideSearchContainer();
      $("body").removeClass("menuOpen");
    }
  });
};

/**
 * Desc: Animation for search bar
 */
const searchBarAnim = () => {
  let tl = gsap.timeline();
  tl.fromTo(
    "#hide-on-search-click",
    {
      x: "0",
      opacity: 1,
      pointerEvents: "initial",
    },
    {
      x: "+60px",
      opacity: 0,
      onComplete: function () {
        this.targets()[0].style.pointerEvents = "none";
      },
    }
  ).fromTo(
    "#search-query-on-desktop",
    {
      x: "0",
      opacity: 0,
      pointerEvents: "none",
    },
    {
      x: "-250px",
      opacity: 1,
      //Enable typing after animation
      onComplete: function () {
        this.targets()[0].style.pointerEvents = "initial";
      },
    },
    "+=.2"
  );
  return tl;
};

/**
 * Desc: add event listeners for showing and hiding search bar input.
 */
const showSearchBar = () => {
  $(".navbar").on("click", ".fa-search", () => {
    if (!isSearchInputOpen) {
      if (window.innerWidth < 1250) {
        $("#search-query").show().focus();
      } else {
        searchBarAnim();
      }
      isSearchInputOpen = true;
    } else {
      if (window.innerWidth < 1250) {
        $("#search-query").hide();
      } else {
        //Reverse the animation from the end, 0 parameter is needed.
        searchBarAnim().reverse(0);
      }
      hideSearchContainer();
      isSearchInputOpen = false;
    }
  });
};

/**
 * Desc: Listen for resize event and change body class
 */
const onResizeWindow = () => {
  $(window).on("resize", () => {
    $(".searchResults-container").hide();
    if ($(".sub-menu").css("opacity") == 1) {
      $(".sub-menu").css("transform", "translateY(-200%)").css("opacity", "0");
      $("#search-query").hide();
      $("body").removeClass("menuOpen");
    }
  });
};

export { openAndCloseMenu, showSearchBar, onResizeWindow };
