import { checkUser, createAndSaveNewUser } from "../data/data.js";
import { hideSearchContainer } from "./search.js";
/**
 * Desc: add 2 event listeners for showing and closing login modal.
 */
export const toggleLoginModal = () => {
  $(".navbar").on("click", ".personal-btn", (e) => {
    //Show login modal
    $(".login-overlay").css("display", "flex");
    //Add or remove active tabs based on the clicked element.
    [...$(".tabs li")].forEach((li) => {
      if (li.innerText == e.target.innerText) {
        li.classList.add("active");
      } else {
        li.classList.remove("active");
      }
    });
    //Display correct form based on the clicked element.
    if (e.target.innerText == "Login") {
      $(".register-form").css("display", "none");
      $(".login-form").css("display", "flex");
    } else {
      $(".login-form").css("display", "none");
      $(".register-form").css("display", "flex");
    }
    if ($(".sub-menu").css("opacity") == 1) {
      // On small screen
      //Close submenu
      $(".sub-menu").css("transform", "translateY(-200%)").css("opacity", "0");
      if ($("body").hasClass("menuOpen")) {
        $("body").removeClass("menuOpen");
      }
      $("#search-query").hide();
    } else {
      // On big screen
      hideSearchContainer();
    }
  });

  $(".login-overlay").on("click", ".exit-modal-btn",() => {
    $(".login-overlay").css("display","none");
  });
};

/**
 * Desc: Change Login or Register Screen when click on tabs.
*/
export const changeModalTab = () => {
  $(".login-overlay").on("click", ".tabs li", function () {
    $(this).addClass('active');
    $(this).siblings().removeClass('active');
    if ($(this).text() == "Login") {
      $(".register-form").css("display", "none");
      $(".login-form").css("display", "flex");
    } else {
      $(".login-form").css("display", "none");
      $(".register-form").css("display", "flex");
    }
  })
}

/**
 * Desc: Change login section display based on log in status of users.
 * In: log in status.
 * Out: Log in section display.
 */
export const displayLoginSection = (isLoggedIn) => {
  displayLoginSectionOnMobile(isLoggedIn);
  displayLoginSectionOnDesktop(isLoggedIn);
};

const displayLoginSectionOnMobile = (isLoggedIn) => {
  let html = "";
  if (isLoggedIn == "true") {
    let username = sessionStorage.getItem("usernameLoggingIn");
    html = `<li class="show-when-logged-in">
                <p class="logged-in-mes">Hello ${username}.</p>
                <button class="log-out-btn personal-btn">Log Out.</button>
              </li>
              `;
  } else {
    html = `<li>
              <a href="#" class="personal-btn login">Login</a>
            </li>
            <li>
              <a href="#" id="register" class="personal-btn cta-btn"
                >Register</a
              >
            </li>`;
  }
  html += `<li>
              <i class="fas fa-search"></i>
              <input
                type="text"
                name="search-query"
                id="search-query"
                placeholder="Find your favorite games"
              />
              <ul class="searchResults-container"></ul>
            </li>`;
  $(".sub-menu .personal-menu").html(html);
};

export const displayLoginSectionOnDesktop = (isLoggedIn) => {
  let html = `<li>
  <div id="hide-on-search-click">Search</div>
  <input
  type="text"
  name="search-query"
  id="search-query-on-desktop"
  placeholder="Find your favorite games"
  />
  <ul class="searchResults-container"></ul>
  <div class="search-btn">
  <i class="fas fa-search"></i>
  </div>
  </li>`;
  if (isLoggedIn == "true") {
    let username = sessionStorage.getItem("usernameLoggingIn");
    html += `<li class="show-when-logged-in">
    <p class="logged-in-mes">Hello ${username}.</p>
    <button class="log-out-btn personal-btn">Log Out.</button>
    </li>`;
  } else {
    html += `<li>
            <a href="#" class="personal-btn login">Login</a>
          </li>
          <li>
            <a href="#" id="register" class="personal-btn cta-btn">Register</a>
          </li>`;
  }
  $("#on-desktop").html(html);
};

/**
 * Desc: Check users credentials.
 */
export const submitLoginForm = () => {
  //Uses onsubmit not onclick to retain form validation
  $(".login-overlay").on("submit", ".login-form", (e) => {
    e.preventDefault();
    let username = $("#username-login").val();
    let password = $("#password-login").val();
    /**
     * If checkuser => show login section with face and info.
     * else have notification
     */
    if (checkUser(username, password)) {
      $(".login-overlay").css("display","none")
      $(".sub-menu").css({
        transform: "translateY(0)",
        opacity: 1,
      });
      if (window.innerWidth >= 1250) {
        $("body").removeClass("menuOpen");
      }

      sessionStorage.setItem("isLoggedIn", true);
      sessionStorage.setItem("usernameLoggingIn", username);
      displayLoginSection(sessionStorage.getItem("isLoggedIn"));
    } else {
      alert("Wrong username or password, please type it again.");
    }
  });
};
  /**
   * Desc: Check existing user and create one if new
   */
export const submitRegisterForm = () => {
    $(".login-overlay").on("submit", ".register-form" ,(e) => {
      
      e.preventDefault();
      let username = $("#username-register").val();
      let password = $("#password-register").val();
      
      if(createAndSaveNewUser(username, password)) {
        sessionStorage.setItem("isLoggedIn", true);
        sessionStorage.setItem("usernameLoggingIn", username);
        displayLoginSection(sessionStorage.getItem("isLoggedIn"));
        window.location.href = "./index.html";
        alert("Successfully Created New User!")
      } else {
        alert("Username already exist, please choose a different username.");
      };
    });
}

/**
 * Desc: When users log out, they will be redirected to homepage.
 */
export const logOut = () => {
  $(".navbar").on("click", (e) => {
    if ($(e.target).hasClass("log-out-btn")) {
      sessionStorage.setItem("isLoggedIn", false);
      $(".login-overlay").css("display",'none');
      window.location.href = "./index.html";
    }
  });
};
