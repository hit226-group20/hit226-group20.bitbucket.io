/**
 * This scripts contains all functionalities for the hero section Slider.
 */

//Global State - keep track of play/pause state for slider
let sliderId;

/**
 * Desc: Switch active onClick or finish animation
 */
const swapState = (el, state, next = undefined) => {
  if (next) {
    if ($(el).next()[0]) {
      $(el).next().addClass(state).siblings().removeClass(state);
    } else {
      let firstClass = el.split(".")[1];
      $(`.${firstClass}`).first().addClass(state).siblings().removeClass(state);
    }
  } else {
    $(el).addClass(state).siblings().removeClass(state);
  }
};

/* Gsap Helper Function */
// Wrap function takes from https://greensock.com/forums/topic/16952-modifiers-plugin-for-infinite-image-carousel/
const wrap = (value, min, max) => {
  var v = value - min;
  var r = max - min;
  return ((r + (v % r)) % r) + min;
};

/**
 * Desc: Switch Slide Animation
 */
const switchSlide = () => {
  gsap.to(".slide", {
    x: "-=1920",
    duration: 2,
    modifiers: {
      //For infinite carousel effect, move element to last when out of bound.
      x: function (x) {
        return `${wrap(parseInt(x), -1920, 3840)}px`;
      },
    },
    onComplete: function () {
      swapState(".page-btn.active", "active", "next");
      swapState(".slide.activeSlide", "activeSlide", "next");
    },
  });
};

/**
 * Desc: Auto slide every 5s.
 */
const autoRunSlide = () => {
  let sliderId = setInterval(() => {
    switchSlide();
  }, 5000);
  return sliderId;
};

sliderId = autoRunSlide();

/**
 * Desc: Set position for each slide.
 */
const setPosition = () => {
  gsap.set(".slide", {
    x: function (i) {
      return i * 1920;
    },
  });
};

/**
 * Desc: Stop and continue auto slide
 */
const stopAndPlaySlide = () => {
  //Play or pause slider
  $(".control-btn").on("click", () => {
    $(".fa-play").toggle();
    $(".fa-pause").toggle();
    if ($(".fa-play").is(":visible")) {
      // Stop the slide
      if (sliderId) {
        clearInterval(sliderId);
        sliderId = undefined;
      }
    } else {
      // Play the slide
      if (!sliderId) {
        sliderId = setInterval(() => {
          switchSlide();
        }, 5000);
      }
    }
  });
};

/**
 * Desc: Pause slider and move to selected slide
 */

const controlSlide = () => {
  $(".page-btn").on("click", function (e) {
    //Bind clicked button to variable.
    const that = $(this);
    //Get clicked slide.
    let clickedSlide = parseInt($(this).attr("data-page"));
    //Current Slide on before switching
    let currentSlide = parseInt($(".activeSlide").attr("id"));

    if (sliderId) {
      //Stop auto slide
      clearInterval(sliderId);
      sliderId = undefined;
      if ($(".fa-play").is(":hidden")) {
        $(".fa-play").toggle();
        $(".fa-pause").toggle();
      }
    }

    // Move to specific slide based on clicked slide and current slide in anim.
    const movetoSlide = (clickedSlide, currentSlide) => {
      let translateX = (currentSlide - clickedSlide) * 1920;
      if (!gsap.isTweening(".slide")) {
        gsap.to(".slide", {
          x: `+=${translateX}px`,
          duration: 0.3,
          modifiers: {
            //Keeping x between -1920 and 3840px bound.
            x: function (x) {
              return `${wrap(parseInt(x), -1920, 3840)}px`;
            },
          },
          onComplete: function () {
            //Set new activeSlide
            swapState(`#${clickedSlide}`, "activeSlide");
            //Set circle button to be active.
            swapState(that, "active");
          },
        });
      }
    };
    movetoSlide(clickedSlide, currentSlide);
  });
};

export { setPosition, stopAndPlaySlide, controlSlide };
