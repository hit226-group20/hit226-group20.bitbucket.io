import {
  openAndCloseMenu,
  showSearchBar,
  onResizeWindow,
} from "./components/navbar.js";

import {
  toggleLoginModal,
  changeModalTab,
  submitLoginForm,
  submitRegisterForm,
  displayLoginSection,
  logOut,
} from "./components/modal.js";

import { searchFunc } from "./components/search.js";

$(document).ready(function () {
  /* Load Navigation Bar and Footer */
  $(".navbar").load("./pages/navbar.min.html nav", () => {
    displayLoginSection(sessionStorage.getItem("isLoggedIn"));
  });
  $(".footer").load("./pages/footer.min.html footer");
  $(".login-overlay").load("./pages/modal.min.html .modal-container");

  /* Navbar */
  openAndCloseMenu();
  showSearchBar();
  onResizeWindow();

  /* Login/Register Modal */
  toggleLoginModal();
  submitLoginForm();
  submitRegisterForm();
  changeModalTab();
  logOut();

  /* Search Functionality */
  searchFunc();
});
