/**
 * Code Written By : Rohan.
 * Final Edited By : Quang Tran.
 * */

import { openAndCloseMenu, showSearchBar, onResizeWindow } from "./components/navbar.js";

import {
  toggleLoginModal,
  changeModalTab,
  submitLoginForm,
  submitRegisterForm,
  displayLoginSection,
  logOut,
} from "./components/modal.js";

import { searchFunc } from "./components/search.js";

$(document).ready(function () {
  var acc = document.getElementsByClassName("accordion");
  var i;

  $(".navbar").load("./pages/navbar.min.html nav", () => {
    displayLoginSection(sessionStorage.getItem("isLoggedIn"));
  });
  $(".footer").load("./pages/footer.min.html footer");
  $(".login-overlay").load("./pages/modal.min.html .modal-container");

  /* Navbar */
  openAndCloseMenu();
  showSearchBar();
  onResizeWindow();

  /* Login/Register Modal */
  toggleLoginModal();
  submitLoginForm();
  submitRegisterForm();
  changeModalTab();
  logOut();

  /*Search*/
  searchFunc();

  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function () {
      /* Toggle between adding and removing the "active" class,
      to highlight the button that controls the panel */
      this.classList.toggle("active");

      /* Toggle between hiding and showing the active panel */
      var panel = this.nextElementSibling;
      if (panel.style.display === "block") {
        panel.style.display = "none";
      } else {
        panel.style.display = "block";
      }
    });
  }

  //Get the button:
  let mybutton = $("#myBtn");

  // When the user scrolls down 20px from the top of the document, show the button

  window.onscroll = () => {
    if (
      document.body.scrollTop > 150 ||
      document.documentElement.scrollTop > 150
    ) {
      mybutton.css("display", "block");
    } else {
      mybutton.css("display", "none");
    }
  };

  // When the user clicks on the button, scroll to the top of the document
  mybutton.on("click", () => {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  });
  // java script has been applied by w3school.//
});
