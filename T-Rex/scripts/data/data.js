//Header Img = 320x480
//Trailer = 1280x720

/** Games Data */
export const data = [
  {
    id: "witcher3",
    name: "Witcher 3 - The Wild Hunt",
    genre: "RPG",
    rating: {
      score: 4.9,
      numRating: 20,
    },
    trailerURL: {
      localSource: "./videos/Witcher 3 Trailer.mp4",
      fallBackSource:
        "https://res.cloudinary.com/dixd5lojp/video/upload/v1619327038/T-Rex%20-%20HIT%20226/Trailers/Witcher_3_Trailer_nlfb2x.mp4",
    },
    shortDesc:
      "The Witcher 3: Wild Hunt is a story-driven next-generation open world role-playing game, set in a troubled and morally indifferent fantasy universe.",
    headerImg:
      "https://res.cloudinary.com/dixd5lojp/image/upload/c_scale,w_320/v1619198735/T-Rex%20-%20HIT%20226/cards/apps.28990.69531514236615003.8f0d03d6-6311-4c21-a151-834503c2901a_mvfuoq.webp",
    About: {
      devs: "CD Projekt Red",
      publishers: "Cd Projekt",
      releasedDate: "19 May 2015.",
      lang: "English, Deutsch & 15 more.",
      longDesc:
        "The Witcher: Wild Hunt is a story-driven open world RPG set in a visually stunning fantasy universe full of meaningful choices and impactful consequences. In The Witcher, you play as professional monster hunter Geralt of Rivia tasked with finding a child of prophecy in a vast open world rich with merchant cities, pirate islands, dangerous mountain passes, and forgotten caverns to explore.<br/><br/>You are The Witcher, professional monster hunter, a killer for hire. Trained from early childhood and mutated to gain superhuman skills, strength and reflexes, witchers are a distrusted counterbalance to the monster-infested world in which they live. You are a drifter, always on the move, following in the footsteps of tragedy to make other people's problems your own - if the pay is good. You are now taking on your most important contract yet: to track down the child of prophecy, a living weapon, a key to save or destroy this world. You will make choices that go beyond good & evil, and you will face their far reaching consequences.",
      screenShot: [
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1619350188/T-Rex%20-%20HIT%20226/screenshot/the-witcher-3-wild-hunt-ps4-playstation-4-3.original_avvnlj.webp",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1619350493/T-Rex%20-%20HIT%20226/screenshot/ss_64eb760f9a2b67f6731a71cce3a8fb684b9af267.1920x1080_a1sftb.webp",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1619350573/T-Rex%20-%20HIT%20226/screenshot/goose_dztcb8.webp",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1619350629/T-Rex%20-%20HIT%20226/screenshot/zJalolE_tbffna.webp",
      ],
    },
    SysReq: {
      PC: {
        min: {
          OS: "Windows 7 / 8 (64bit)",
          Processor:
            "Intel CPU Core i5-2500K 3.3 GHz or AMD CPU Phenom II X4 940",
          Memory: "6 GB RAM",
          Graphics: "Nvidia GPU GeForce GTX 660 / AMD GPU Radeon HD 7870",
          DirectX: "11",
          Storage: "35 GB of available space",
        },
        rec: {
          OS: "Windows (64-bit) 7 or (64-bit) 8 (8.1)",
          Processor:
            "Intel CPU Core i7 3770 3.4 GHz / AMD CPU AMD FX-8350 4 GHz",
          Memory: "8 GB RAM",
          Graphics: "Nvidia GPU GeForce GTX 770 / AMD GPU Radeon R9 290",
          DirectX: "11",
          Storage: "35 GB of available space<",
        },
      },
      Xbox: "",
      PS5: "",
    },
  },

  {
    id: "fortnite",
    name: "Fortnite",
    genre: "Action",
    rating: {
      score: 4.5,
      numRating: 10,
    },
    trailerURL: {
      localSource: "./videos/Fortnite Trailer.mp4",
      fallBackSource:
        "https://res.cloudinary.com/dixd5lojp/video/upload/v1620699543/T-Rex%20-%20HIT%20226/Trailers/Fortnite_Trailer_oocvxr.mp4",
    },
    shortDesc:
      "A survival game where 100 players fight against each other in player versus player combat to be the last one standing.",
    headerImg:
      "https://res.cloudinary.com/dixd5lojp/image/upload/c_scale,w_320/v1619199312/T-Rex%20-%20HIT%20226/cards/apps.17944.70702278257994163.a2510aae-a0f0-49d3-8a56-9df543f2d18c_pblcb9.webp",
    About: {
      devs: "Epic Games",
      publishers: "Epic Games",
      releasedDate: "25 Jul, 2017",
      lang: "English, French, German & 9 more.",
      longDesc:
        "Fortnite is a world of many experiences. Drop onto the Island and compete to be the last player — or team — standing. Hang out with friends to catch a concert or a movie. Create a world of your own with your own rules. Or Save the World by taking down hordes of monsters with others.<br><br/>There are four major game modes in Fortnite, together offering something for every kind of player. These four modes are: Battle Royale, Party Royale, Creative, and Save the World.<br><br/>Available on consoles, PC, and mobile, Fortnite Battle Royale is free to download and play! Hop off the Battle Bus to the Island below and battle it out to be the last one remaining. Or team up with friends or other players to outlast other teams. Use the power of wood, brick, and metal to build structures to help you out. There’s always something to look forward to in Fortnite Battle Royale, including new items, weapons, vehicles, submodes, special events, and “Seasons.” Each new Season brings a distinct theme to the Island, like the past secret agent takeover and giant flood.<br><br/>Party Royale is a mode within Battle Royale. Instead of battling, Party Royale is an experimental and evolving space that focuses on no sweat, all chill fun. Attractions include aerial obstacle courses, boat races, movies, and even live concerts from top artists! Also, like all other modes within Battle Royale, Party Royale is free.<br><br/>Included free with Battle Royale, Fortnite Creative puts you in charge of your own Island — create your own games and rules… and invite your friends! Creative gives you the tools to design games both simple and complex, so make what you want to create. And when you’re not creating, experience games made by your friends or others in the Fortnite community. Besides making and playing games, Creative is also a great place for just creating your own scenery. Make your Island how you want it to look and enjoy it with your friends!<br><br/>In Fortnite Save the World, explore a vast, destructible world as you and other players team up to hold back hordes of monsters. In your fight against these monsters — known as Husks — become stronger by finding loot, crafting weapons, and expanding your collection of Heroes. Build a base and fortify it with traps to keep the Husks at bay! Play Save the World with friends or others in the Fortnite community. You can access Save the World by purchasing a Save the World Pack, which you can find on our website, inside Fortnite, or on your console's storefront. Save the World is available on PlayStation, Xbox, and PC.",
      screenShot: [
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1620447579/T-Rex%20-%20HIT%20226/screenshot/Fortnite_2Fblog_2Fcreative_2FCM07_News_Featured_CreativeMode_Announce-1920x1080-f2b3606efe82d43a4a89ba8efbb00b630641e754_erlqm8.webp",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1620447644/T-Rex%20-%20HIT%20226/screenshot/rfZUAmoKbf2uPifH6JAc5S_aro7dd.webp",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1620447756/T-Rex%20-%20HIT%20226/screenshot/Fortnite_2Fchapter2_2F19_1011_Upgraded_Combat_Screenshot-1920x1080-b7fb7387b1402312bbbe8fbba6954e1715095059_rm4ina.webp",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1620447815/T-Rex%20-%20HIT%20226/screenshot/3359863-fortnite_20screenshot_202018.03.05_20-_2012.54.43.13_g8vvcr.webp",
      ],
    },
    SysReq: {
      PC: {
        min: {
          OS: "Windows 7/8/10 64-bit or Mac OS Mojave 10.14.6",
          Processor: "Core i3-3225 3.3 GHz",
          Memory: "4GB RAM",
          Graphics:
            "ntel HD 4000 on PC or Intel Iris Pro 5200 or equivalent AMD GPU on Mac",
          DirectX: "11",
          Storage: "30GB",
        },
        rec: {
          OS: "Windows 7/8/10 64-bit or Mac OS Mojave 10.14.6",
          Processor: "Core i5-7300U 3.5 GHz",
          Memory: "8 GB RAM",
          Graphics:
            "NVIDIA GeForce GTX 660 or AMD Radeon HD 7870 or equivalent DX11 GPU",
          DirectX: "11",
          Storage: "60GB",
        },
      },
      Xbox: "",
      PS5: "",
    },
  },

  {
    id: "acvahalla",
    name: "Assassin's Creed Valhalla",
    genre: "RPG",
    rating: {
      score: 5,
      numRating: 15,
    },
    trailerURL: {
      localSource: "./videos/AC Vahalla Trailer.mp4",
      fallBackSource:
        "https://res.cloudinary.com/dixd5lojp/video/upload/v1620699541/T-Rex%20-%20HIT%20226/Trailers/AC_Vahalla_Trailer_wfs3yy.mp4",
    },
    shortDesc:
      "England in the age of the Vikings is a fractured nation of petty lords and warring kingdoms. Beneath the chaos lies a rich and untamed land waiting for a new conqueror. Will it be you?",
    headerImg:
      "https://res.cloudinary.com/dixd5lojp/image/upload/c_scale,q_100,w_320/v1619199397/T-Rex%20-%20HIT%20226/cards/ac_valhalla__pc_1611910216_e9909230_progressive_jxybpi.webp",
    About: {
      devs: "Ubisoft Montreal",
      publishers: "Ubisoft",
      releasedDate: "11 Nov, 2020",
      lang: "English, French, Italian, German & 10 more.",
      longDesc:
        "Become Eivor, a Viking raider raised to be a fearless warrior, and lead your clan from icy desolation in Norway to a new home amid the lush farmlands of ninth-century England. Find your settlement and conquer this hostile land by any means to earn a place in Valhalla.<br><br/>Blaze your own path across England with advanced RPG mechanics. Fight brutal battles, lead fiery raids or use strategy and alliances with other leaders to bring victory. Every choice you make in combat and conversation is another step on the path to greatness.<br><br/>Lead a crew of raiders and launch lightning-fast surprise attacks against Saxon armies and fortresses. Claim the riches of your enemies' lands for your clan and expand your influence far beyond your growing settlement.<br><br/>Unleash the ruthless fighting style of a Viking warrior as you dual-wield axes, swords, or even shields against relentless foes. Decapitate opponents in close-quarters combat, riddle them with arrows, or assassinate them with your Hidden Blade.<br><br/>Your clan's new home grows with your legend. Customise your settlement by building upgradable structures. Unlock new features and quests by constructing a barracks, a blacksmith, a tattoo parlour, and much more.<br><br/>Recruit mercenary Vikings designed by other players or create and customise your own to share online. Sit back and reap the rewards when they fight alongside your friends in their game worlds.<br><br/>Sail across the icy North Sea to discover and conquer the broken kingdoms of England. Immerse yourself in activities like hunting and drinking games or engage in traditional Norse competitions like flyting – or, as it's better known, verbally devastating rivals through the art of the Viking rap battle.",
      screenShot: [
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1620449595/T-Rex%20-%20HIT%20226/screenshot/iNiU9epDmKD5WSerpTpL9h_ej3yku.webp",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1620449692/T-Rex%20-%20HIT%20226/screenshot/Assassin_s-Creed-Valhalla-Review-Screenshot-2-11082020_oyaz92.webp",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1620449738/T-Rex%20-%20HIT%20226/screenshot/assassins-creed-valhalla-settlement_wv71u3.webp",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1620650914/T-Rex%20-%20HIT%20226/screenshot/000059073_engb_Inventory_dclhf1.webp",
      ],
    },
    SysReq: {
      PC: {
        min: {
          OS: "Windows 10 (64-bit only)",
          Processor: "Ryzen 3 1200 - 3.1 Ghz / i5-4460 - 3.2 Ghz",
          Memory: "8 GB (Dual-channel mode)",
          Graphics: "AMD R9 380 - 4GB / GeForce GTX 960 4GB",
          DirectX: "12",
          Storage: "50 GB HDD (SSD Recommended)",
        },
        rec: {
          OS: "Windows 10 (64-bit only)",
          Processor: "Ryzen 5 1600 - 3.2 Ghz / i7-4790 - 3.6 Ghz",
          Memory: "8 GB (Dual-channel mode)",
          Graphics: "AMD RX 570 - 8GB / GeForce GTX 1060 - 6GB",
          DirectX: "12",
          Storage: "50 GB HDD (SSD Recommended)",
        },
      },
      Xbox: "",
      PS5: "",
    },
  },

  {
    id: "gtav",
    name: "Grand Theft Auto V",
    genre: "RPG",
    rating: {
      score: 5,
      numRating: 50,
    },
    trailerURL: {
      localSource: "./videos/Grand Theft Auto V Trailer.mp4",
      fallBackSource:
        "https://res.cloudinary.com/dixd5lojp/video/upload/v1620699540/T-Rex%20-%20HIT%20226/Trailers/Grand_Theft_Auto_V_Trailer_fhpaer.mp4",
    },
    shortDesc:
      "When a young street hustler, a retired bank robber and a terrifying psychopath land themselves in trouble, they must pull off a series of dangerous heists to survive in a city in which they can trust nobody, least of all each other.",
    headerImg:
      "https://res.cloudinary.com/dixd5lojp/image/upload/q_100/v1619199507/T-Rex%20-%20HIT%20226/cards/apps.32034.68565266983380288.0f5ef871-88c0-45f7-b108-6aacbc041fcf_a0jpnw.webp",
    About: {
      devs: "Rockstar Games",
      publishers: "Rockstar Games",
      releasedDate: "17 Sep, 2013",
      lang: "English, French, Italian & 10 more.",
      longDesc:
        "When a young street hustler, a retired bank robber and a terrifying psychopath find themselves entangled with some of the most frightening and deranged elements of the criminal underworld, the U.S. government and the entertainment industry, they must pull off a series of dangerous heists to survive in a ruthless city in which they can trust nobody, least of all each other.<br><br/>Grand Theft Auto V for PC offers players the option to explore the award-winning world of Los Santos and Blaine County in resolutions of up to 4k and beyond, as well as the chance to experience the game running at 60 frames per second.<br><br/>The game offers players a huge range of PC-specific customization options, including over 25 separate configurable settings for texture quality, shaders, tessellation, anti-aliasing and more, as well as support and extensive customization for mouse and keyboard controls. Additional options include a population density slider to control car and pedestrian traffic, as well as dual and triple monitor support, 3D compatibility, and plug-and-play controller support.<br><br/>Grand Theft Auto V for PC also includes Grand Theft Auto Online, with support for 30 players and two spectators. Grand Theft Auto Online for PC will include all existing gameplay upgrades and Rockstar-created content released since the launch of Grand Theft Auto Online, including Heists and Adversary modes.<br><br/>The PC version of Grand Theft Auto V and Grand Theft Auto Online features First Person Mode, giving players the chance to explore the incredibly detailed world of Los Santos and Blaine County in an entirely new way.<br><br/>Grand Theft Auto V for PC also brings the debut of the Rockstar Editor, a powerful suite of creative tools to quickly and easily capture, edit and share game footage from within Grand Theft Auto V and Grand Theft Auto Online. The Rockstar Editor’s Director Mode allows players the ability to stage their own scenes using prominent story characters, pedestrians, and even animals to bring their vision to life. Along with advanced camera manipulation and editing effects including fast and slow motion, and an array of camera filters, players can add their own music using songs from GTAV radio stations, or dynamically control the intensity of the game’s score. Completed videos can be uploaded directly from the Rockstar Editor to YouTube and the Rockstar Games Social Club for easy sharing.",
      screenShot: [
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1620650582/T-Rex%20-%20HIT%20226/screenshot/gta-5-ps5_whvlh1.webp",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1620450631/T-Rex%20-%20HIT%20226/screenshot/gtav-comp-9-1_eoywje.webp",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1620450674/T-Rex%20-%20HIT%20226/screenshot/latest_io75lf.webp",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1620450732/T-Rex%20-%20HIT%20226/screenshot/GTA-V-Was-Pulled-from-Target-in-Australia-Looks-Like-a-Win-for-the-Dark-Side-466680-2_tbtobw.webp",
      ],
    },
    SysReq: {
      PC: {
        min: {
          OS: "Windows 10 64 Bit, Windows 8.1 64 Bit, Windows 8 64 Bit, Windows 7 64 Bit Service Pack 1",
          Processor:
            "Intel Core 2 Quad CPU Q6600 @ 2.40GHz (4 CPUs) / AMD Phenom 9850 Quad-Core Processor (4 CPUs) @ 2.5GHz",
          Memory: "4 GB RAM",
          Graphics: "NVIDIA 9800 GT 1GB / AMD HD 4870 1GB",
          DirectX: "10",
          Storage: "72 GB available space",
        },
        rec: {
          OS: "Windows 10 64 Bit, Windows 8.1 64 Bit, Windows 8 64 Bit, Windows 7 64 Bit Service Pack 1",
          Processor:
            "Intel Core i5 3470 @ 3.2GHz (4 CPUs) / AMD X8 FX-8350 @ 4GHz (8 CPUs)",
          Memory: "8 GB RAM",
          Graphics: "NVIDIA GTX 660 2GB / AMD HD 7870 2GB",
          DirectX: "10",
          Storage: "72 GB available space",
        },
      },
      Xbox: "",
      PS5: "",
    },
  },
  {
    id: "aoe3",
    name: "Age of Empires III",
    genre: "Strategy",
    rating: {
      score: 3.4,
      numRating: 30,
    },
    trailerURL: {
      localSource: "./videos/Age of Empires III Trailer.mp4",
      fallBackSource:
        "https://res.cloudinary.com/dixd5lojp/video/upload/v1620699545/T-Rex%20-%20HIT%20226/Trailers/Age_of_Empires_III_Trailer_z28ztb.mp4",
    },
    shortDesc:
      "Command mighty civilizations from across Europe and the Americas or jump to the battlefields of Asia in stunning 4K Ultra HD graphics and with a fully remastered soundtrack.",
    headerImg:
      "https://res.cloudinary.com/dixd5lojp/image/upload/q_100/v1619199656/T-Rex%20-%20HIT%20226/cards/apps.44836.13905755391310011.6681c157-530b-4476-b20e-6cf142a9a94e_g09zdo.webp",
    About: {
      devs: "Tantalus Media, Forgotten Empires",
      publishers: "Xbox Game Studios",
      releasedDate: "18 October, 2005",
      lang: "English, French, Italian and 15 more.",
      longDesc:
        "Age of Empires III: Definitive Edition completes the celebration of one of the most beloved real-time strategy franchises in definitive form with enhanced features and modernized gameplay.<br><br/>Command mighty civilizations from across Europe and the Americas or jump to the battlefields of Asia in stunning 4K Ultra HD graphics and with a fully remastered soundtrack.<br><br/>Now featuring two new game modes: Historical Battles and The Art of War Challenge Missions, including all previously released expansions and all 14 civilizations, plus two brand new civilizations – the Swedes and Inca.<br><br/>Head online to challenge other players with updated online multiplayer with cross network play and enjoy modern gaming features including spectator modes and mod support.<br><br/>Complete your collection with this final chapter in the Age of Empires Definitive Edition journey.",
      screenShot: [
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1620452557/T-Rex%20-%20HIT%20226/screenshot/Xe5e3XSK4eyvMuJrSKHafY_wt7o6a.webp",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1620452646/T-Rex%20-%20HIT%20226/screenshot/ageempires3_3272163b_z9m3uj.webp",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1620651050/T-Rex%20-%20HIT%20226/screenshot/age-of-empires-3-definitive-edition-unveils-new-civilizations-and-its-release-date_ftgtct.webp",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1620452787/T-Rex%20-%20HIT%20226/screenshot/p1_2549720_5f2ce34c_fpj0ez.webp",
      ],
    },
    SysReq: {
      PC: {
        min: {
          OS: "Windows 10 version 18362.0 or higher",
          Processor:
            "Intel i3-2105 @ 3.1GHz or AMD Phenom II X4 973 with an average CPU Passmark score of 3735 or better",
          Memory: "8 GB RAM",
          Graphics:
            "GeForce GT 430, Radeon HD 5570, or Intel HD 4400 with an average Passmark G3D Mark of 570 or better @720p",
          DirectX: "11",
          Storage: "42 GB available space",
        },
        rec: {
          OS: "Windows 10 version 18362.0 or higher",
          Processor:
            "Intel i5-3300 @ 3.0GHz or AMD FX-8350 or equivalent with an average CPU Passmark score of 4100 or better",
          Memory: "16 GB RAM",
          Graphics:
            "GeForce GTX 980 or Radeon R9 Fury or equivalent with an average Passmark G3D Mark of 9500 or better @1080p",
          DirectX: "11",
          Storage: "42 GB available space",
        },
      },
      Xbox: "",
      PS5: "",
    },
  },
  {
    id: "halo",
    name: "Halo - Masterchief Collection",
    genre: "Stealth",
    rating: {
      score: 4.5,
      numRating: 14,
    },
    trailerURL: {
      localSource: "./videos/Halo- The Master Chief Collection Trailer.mp4",
      fallBackSource:
        "https://res.cloudinary.com/dixd5lojp/video/upload/v1620699545/T-Rex%20-%20HIT%20226/Trailers/Halo-_The_Master_Chief_Collection_Trailer_bvj2lx.mp4",
    },
    shortDesc:
      "The Master Chief’s iconic journey includes six games, built for PC and collected in a single integrated experience. Whether you’re a long-time fan or meeting Spartan 117 for the first time, The Master Chief Collection is the definitive Halo gaming experience.",
    headerImg:
      "https://res.cloudinary.com/dixd5lojp/image/upload/v1619232256/T-Rex%20-%20HIT%20226/cards/cover_iikc9u.webp",
    About: {
      devs: "343 Industries, Splash Damage, Ruffian Games, Bungie, Saber Interactive",
      publishers: "Xbox Game Studios",
      releasedDate: "11 November, 2014",
      lang: "English, French, Italian and 14 more.",
      longDesc:
        "The series that changed console gaming forever is on PC with six blockbuster games in one epic experience.<br><br/> Season 6 is available now as the latest FREE update for Halo: The Master Chief Collection! Enjoy new content like:<br><br/>• New Halo 3 Armors: Inspired by the Halo: Fireteam Raven arcade game, players can unlock four new armor sets, vehicle skins and weapon skins styled after the legendary strike team.<br><br/>• New Customization options: Create a Spartan that’s uniquely yours with all new cosmetic effects like animated visors and back accessories for Halo 3 alongside over 100 additional Season 6 rewards.<br><br/>• Season Points Exchange: Exchange season points for exclusive new cosmetic items as well as prior seasonal challenge rewards.<br><br/>PC Settings/Optimizations: Halo: The Master Chief Collection is now optimized for PC and looking better than ever at up to 4k UHD and at 60+ FPS.* Other setting options include customizable mouse and keyboard support, ultrawide support, FOV customization, and more.<br><br/>Campaign: Featuring Halo: Reach, Halo: Combat Evolved Anniversary, Halo 2: Anniversary, Halo 3, Halo 3: ODST Campaign, and Halo 4, The Master Chief Collection offers players their own exciting journey through the epic saga. Starting with the incredible bravery of Noble Six in Halo: Reach and ending with the rise of a new enemy in Halo 4, the Master Chief’s saga totals 67 campaign missions over six critically-acclaimed titles.<br><br/>Multiplayer: Each of the six games in The Master Chief Collection brings its own multiplayer maps, modes and game types. With more than 120 multiplayer maps and countless ways to play with community-created Forge content the Collection has the most diverse and expansive Halo multiplayer experience to date.<br><br/>Forge: Halo’s iconic map editor is improved, refreshed and better than ever. Build new maps with expanded functionality, increased budget, new objects and create new ways to play with custom game modes.**<br><br/>*Look to system requirements for guidance on hardware minimum specs to achieve performance metrics.<br><br/>**Forge mode not available in Halo: Combat Evolved Anniversary or Halo 3: ODST..",
      screenShot: [
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1620521975/T-Rex%20-%20HIT%20226/screenshot/ss_62bbd86f4735893ef6cd53206cf8c93f87eb86ec.1920x1080_ep4oqp.webp",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1620522007/T-Rex%20-%20HIT%20226/screenshot/master-chief-collection_eu1jan.webp",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1620522034/T-Rex%20-%20HIT%20226/screenshot/halo_mcc_pc_release_date_trailer_onvgxv.webp",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1620522109/T-Rex%20-%20HIT%20226/screenshot/57G8BEyYqrwNVkWX_upl7ca.webp",
      ],
    },
    SysReq: {
      PC: {
        min: {
          OS: "Windows 7 (64-bit)",
          Processor: "AMD Phenom II X4 960T ; Intel i3550",
          Memory: "8GB RAM",
          Graphics: "AMD HD 6850 ; NVIDIA GeForce GTS 450",
          DirectX: "11",
          Storage: "43 GB available space",
        },
        rec: {
          OS: "WIndows 10 (64-bit)",
          Processor: "AMD FX-4100 / Intel Core i7-870",
          Memory: "8GB RAM",
          Graphics: "AMD Radeon R7 360 ; NVIDIA GeForce GTX 560 T",
          DirectX: "11",
          Storage: "43 GB available space",
        },
      },
      Xbox: "",
      PS5: "",
    },
  },
  {
    id: "hitman3",
    name: "Hitman III",
    genre: "Stealth",
    rating: {
      score: 4.7,
      numRating: 24,
    },
    trailerURL: {
      localSource: "./videos/Hitman 3 Trailer.mp4",
      fallBackSource:
        "https://res.cloudinary.com/dixd5lojp/video/upload/v1621165208/T-Rex%20-%20HIT%20226/Trailers/Hitman_3_Trailer_uf7g9z.mp4",
    },
    shortDesc:
      "Death Awaits. Agent 47 returns in HITMAN 3, the dramatic conclusion to the World of Assassination trilogy.",
    headerImg:
      "https://res.cloudinary.com/dixd5lojp/image/upload/v1619232352/T-Rex%20-%20HIT%20226/cards/apps.42682.14604786804849616.ed0c8857-b959-450a-8982-01dd5a264480_cd4pfa.webp",
    About: {
      devs: "IO Interactive A/S",
      publishers: "IO Interactive A/S",
      releasedDate: "20 Jan, 2021",
      lang: "English, French, Italian, German, Spanish - Spain",
      longDesc:
        "Agent 47 returns as a ruthless professional in HITMAN 3 for the most important contracts of his entire career. Embark on an intimate journey of darkness and hope in the dramatic conclusion to the World of Assassination trilogy. Death awaits.<br><br/>Experience a globetrotting adventure and visit exotic locations that are meticulously detailed and packed full of creative opportunities. IOI’s award-winning Glacier technology powers HITMAN 3’s tactile and immersive game world to offer unparalleled player choice and replayability.<br><br/>HITMAN 3 is the best place to play every game in the World of Assassination trilogy. All locations from HITMAN 1 and HITMAN 2 can be imported and played within HITMAN 3 at no additional cost for existing owners – plus progression from HITMAN 2 is directly carried over into HITMAN 3 at launch.",
      screenShot: [
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1621165548/T-Rex%20-%20HIT%20226/screenshot/hitman3review_3360233b_fzldqm.png",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1621165592/T-Rex%20-%20HIT%20226/screenshot/80_avcnix.jpg",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1621165676/T-Rex%20-%20HIT%20226/screenshot/Hitman-3-death-in-the-family_odybqd.jpg",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1621165711/T-Rex%20-%20HIT%20226/screenshot/hitman-3-there-was-a-fire-fight-guide-hero_feature_hfdmu3.jpg",
      ],
    },
    SysReq: {
      PC: {
        min: {
          OS: "Windows 10 64-bit",
          Processor: "Intel Core i5-2500K / AMD Phenom II X4 940",
          Memory: "8 GB",
          Graphics: "NVIDIA GeForce GTX 660 / AMD Radeon HD 7870",
          DirectX: "12",
          Storage: "80 GB",
        },
        rec: {
          OS: "Windows 10 64-bit",
          Processor: "Intel Core i7-4790 / AMD Ryzen 5 1600",
          Memory: "16 GB",
          Graphics: "NVIDIA GeForce GTX 1070 / AMD Radeon RX Vega 56",
          DirectX: "12",
          Storage: "80 GB",
        },
      },
      Xbox: "",
      PS5: "",
    },
  },
  {
    id: "cyberpunk2077",
    name: "Cyperpunk 2077",
    genre: "RPG",
    rating: {
      score: 2.5,
      numRating: 70,
    },
    trailerURL: {
      localSource: "./videos/Cyberpunk 2077 Trailer.mp4",
      fallBackSource:
        "https://res.cloudinary.com/dixd5lojp/video/upload/v1621167058/T-Rex%20-%20HIT%20226/Trailers/Cyberpunk_2077_Trailer_kufeiw.mp4",
    },
    shortDesc:
      "Cyberpunk 2077 is an open-world, action-adventure story set in Night City, a megalopolis obsessed with power, glamour and body modification.",
    headerImg:
      "https://res.cloudinary.com/dixd5lojp/image/upload/v1619232501/T-Rex%20-%20HIT%20226/cards/cover.cyberpunk-2077.720x1080.2019-08-20.154_cq3bsp.webp",
    About: {
      devs: "CD Projekt Red",
      publishers: "CD Projekt Red",
      releasedDate: "10 Dec, 2020",
      lang: "English, Deutsch & 18 more.",
      longDesc:
        "Cyberpunk 2077 is an open-world, action-adventure story set in Night City, a megalopolis obsessed with power, glamour and body modification. You play as V, a mercenary outlaw going after a one-of-a-kind implant that is the key to immortality. You can customize your character's cyberware, skillset and playstyle, and explore a vast city where the choices you make shape the story and the world around you.<br><br/>Become a cyberpunk, an urban mercenary equipped with cybernetic enhancements and build your legend on the streets of Night City. Create your character from scratch and choose their background out of three unique Lifepaths. Take the role of a gang-wise Street Kid, freedom-loving Nomad, or a ruthless Corpo.<br><br/>Enter the massive open world of Night City, a place that sets new standards in terms of visuals, complexity and depth. Explore the bustling megalopolis of the future and its extensive districts, each with exceptional visual flavor, inhabitants and chances to earn cash. Interact with members of powerful gangs who rule the streets of Night City.<br><br/>Take the riskiest job of your life and go after a prototype implant that is the key to immortality. Guided by the legendary Rockerboy, Johnny Silverhand (played by Keanu Reeves), you will change the course of the corporate-ruled world forever. All this while listening to an intoxicating soundtrack from artists like Run the Jewels, Refused, Grimes, A$AP Rocky, Gazelle Twin, Ilan Rubin, Richard Devine, Nina Kraviz, Deadly Hunta, Rat Boy, and Tina Guo.",
      screenShot: [
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1621167270/T-Rex%20-%20HIT%20226/screenshot/image_stream-39120-2618_0002_ocoqnn.jpg",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1621167318/T-Rex%20-%20HIT%20226/screenshot/00hYjpCCgBH5VFYPsZrBqmf-1..1600444074_nsojuo.jpg",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1621167374/T-Rex%20-%20HIT%20226/screenshot/cyberpunk-2-3_ouauf3.jpg",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1621167423/T-Rex%20-%20HIT%20226/screenshot/Cyberpunk-2077-Jackie-eating-noodles_eumk0u.jpg",
      ],
    },
    SysReq: {
      PC: {
        min: {
          OS: "Windows 7 or 10 (64-bit)",
          Processor: "Intel Core i5-3570K / AMD FX-8310",
          Memory: "8GB RAM",
          Graphics: "GTX 780 / Radeon RX 470",
          DirectX: "12",
          Storage: "70 GB",
        },
        rec: {
          OS: "Windows 10 (64-bit)",
          Processor: "Intel Core i7-4790 / AMD Ryzen 3 3200G",
          Memory: "12 GB RAM",
          Graphics: "GTX 1060 6GB, GTX 1660 Super or Radeon RX 590",
          DirectX: "12",
          Storage: "70 GB",
        },
      },
      Xbox: "",
      PS5: "",
    },
  },
  {
    id: "diablo3",
    name: "Diablo 3",
    genre: "RPG",
    rating: {
      score: 4.5,
      numRating: 30,
    },
    trailerURL: {
      localSource: "",
      fallBackSource:
        "https://res.cloudinary.com/dixd5lojp/video/upload/v1621749280/T-Rex%20-%20HIT%20226/Trailers/Diablo_III_Opening_Cinematic_xp3usd.mp4",
    },
    shortDesc:
      "It has been said that in the end of all things, we would find a new beginning. But as the shadow once again crawls across our world and the stench of terror drifts on a bitter wind, people pray for strength and guidance.",
    headerImg:
      "https://res.cloudinary.com/dixd5lojp/image/upload/v1619240210/T-Rex%20-%20HIT%20226/small%20cards/game-card_diablo3_enUS_ophhl3.webp",
    About: {
      devs: "Blizzard North, Team 3",
      publishers: "Blizzard Entertainment",
      releasedDate: "15 May, 2012 (PC/Mac)",
      lang: "English, French, German and 7 more.",
      longDesc:
        "The game takes place in Sanctuary, the dark fantasy world of the Diablo series, twenty years after the events of Diablo II. Deckard Cain and his niece Leah are in the Tristram Cathedral investigating ancient texts regarding an ominous prophecy. Suddenly, a mysterious star falling from the sky strikes the Cathedral, creating a deep crater into which Deckard Cain disappears.<br><br>The player character, known as the Nephalem, arrives in New Tristram to investigate the fallen star. The Nephalem rescues Cain upon Leah's request and discovers that the fallen object is actually a person. The stranger has no memories except that he lost his sword, which was shattered into three pieces. Although the Nephalem retrieves the pieces, the witch Maghda seizes the shards and attempts to capture Cain to force him to repair the sword for her own ends. However, with an uncontrolled display of power, Leah forces Maghda to flee, and she kidnaps the stranger instead. Cain, dying from Maghda's torture, uses the last of his strength to repair the sword and instructs the Nephalem to return it to the stranger. The Nephalem rescues the stranger and returns his sword, causing him to regain his memories. The stranger then reveals himself as the fallen angel Tyrael. Disgusted with his fellow angels' unwillingness to protect humanity from the forces of Hell, Tyrael cast aside his divinity to become a mortal and warn Sanctuary about the arrival of the demon lords Belial and Azmodan.<br><br>To avenge Cain's death, the Nephalem tracks Maghda to the city of Caldeum, which is controlled by her master, Belial. The Nephalem kills Maghda, and rescues Leah's mother, Adria. Adria tells Tyrael and the Nephalem that the key to stopping the demons is the Black Soulstone, which can trap the souls of the seven Lords of Hell and destroy them forever. In order to obtain the Black Soulstone, the Nephalem resurrects the mad Horadrim, Zoltun Kulle. Kulle reveals its hiding place and completes the unfinished Soulstone, but is killed by the Nephalem after he attempts to steal it for himself. The Nephalem kills Belial and traps his soul within the Black Soulstone, freeing Caldeum. As Leah studies in Caldeum's library to find more answers about the Black Soulstone and Azmodan, she receives a vision from Azmodan, who tells her that he is sending an army from the ruins of Mount Arreat to take the Black Soulstone for himself.<br><br><b>You can read the full description here <a href='#' class='ref'>Diablo 3 Wiki</a></b>",
      screenShot: [
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1621749423/T-Rex%20-%20HIT%20226/screenshot/DIA_SwitchPR_InGame_4PlayerParty_TF_008_png_jpgcopy_penmnq.jpg",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1621749508/T-Rex%20-%20HIT%20226/screenshot/Diablo-3-1_trezki.jpg",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1621749576/T-Rex%20-%20HIT%20226/screenshot/Wallpaper024-1920x1080__281_29_vrvsgm.jpg",
        "https://res.cloudinary.com/dixd5lojp/image/upload/v1621749744/T-Rex%20-%20HIT%20226/screenshot/diablo-iii-wallpaper-5_qhgwsv.jpg",
      ],
    },
    SysReq: {
      PC: {
        min: {
          OS: "Windows7 / Windows 8 / Windows 10",
          Processor: "Intel® Core™ 2 Duo or AMD Athlon™ 64 X2",
          Memory: "4GB RAM",
          Graphics:
            "NVIDIA® GeForce® 8800GT or ATI Radeon™ HD 2900 XT or Intel® HD Graphics 4000",
          DirectX: "11",
          Storage: "25GB",
        },
        rec: {
          OS: "Windows 10 64-bit",
          Processor:
            "Intel® Core™ 2 Duo 2.4 GHz or AMD Athlon™ 64 X2 5600+ 2.8GHz",
          Memory: "6GB RAM",
          Graphics:
            "NVIDIA® GeForce® GTX™ 260 or ATI Radeon™ HD 4870 or better",
          DirectX: "11",
          Storage: "25GB",
        },
      },
      Xbox: "",
      PS5: "",
    },
  },
];

/* create new user if not exist based on credentials input */
export const createAndSaveNewUser = (username, password) => {
  let newUser = JSON.stringify({ username, password });
  if (!sessionStorage.getItem(`user ${username}`)) {
    sessionStorage.setItem(`user ${username}`, newUser);
    return true;
  } else {
    return false;
  }
};

/* Check username and password */
export const checkUser = (username, password) => {
  let existingUser;
  if (sessionStorage.getItem(`user ${username}`)) {
    existingUser = JSON.parse(sessionStorage.getItem(`user ${username}`));
  }
  if (
    existingUser?.username == username &&
    existingUser?.password == password
  ) {
    return true;
  } else {
    return false;
  }
};
