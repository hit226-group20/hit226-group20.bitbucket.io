import {
  openAndCloseMenu,
  showSearchBar,
  onResizeWindow,
} from "./components/navbar.min.js";

import {
  toggleLoginModal,
  changeModalTab,
  submitLoginForm,
  submitRegisterForm,
  displayLoginSection,
  logOut,
} from "./components/modal.min.js";

import {
  setPosition,
  controlSlide,
  stopAndPlaySlide,
} from "./components/heroSlider.min.js";
import { searchFunc } from "./components/search.min.js";

$(document).ready(function () {
  //Link relative to the html file not JS file
  $(".navbar").load("./pages/navbar.min.html nav", () => {
    displayLoginSection(sessionStorage.getItem("isLoggedIn"));
  });
  $(".footer").load("./pages/footer.min.html footer");
  $(".login-overlay").load("./pages/modal.min.html .modal-container");

  /* Navbar */
  openAndCloseMenu();
  showSearchBar();
  onResizeWindow();

  /* Login/Register Modal */
  toggleLoginModal();
  submitLoginForm();
  submitRegisterForm();
  changeModalTab();
  logOut();

  /*Search*/
  searchFunc();

  /* Hero Slider*/
  setPosition();
  stopAndPlaySlide();
  controlSlide();

  /* Card Component */
  $(".submitAnchorTag").on("click", function () {
    $(this).closest("form").submit();
  });

  // Hide play button by default.
  $(".fa-play").hide();

  //Change Category colors
  [...$(".category")].forEach((ele) => {
    ele.classList.add(`${ele.innerText}`);
  });
});
