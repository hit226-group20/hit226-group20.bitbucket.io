import {
  openAndCloseMenu,
  showSearchBar,
  onResizeWindow,
} from "./components/navbar.js";

import {
  toggleLoginModal,
  changeModalTab,
  submitLoginForm,
  submitRegisterForm,
  displayLoginSection,
  logOut,
} from "./components/modal.js";

import { searchFunc } from "./components/search.js";
import { data } from "./data/data.js";

$(document).ready(function () {
  //Link relative to the html file not JS file
  $(".navbar").load("./pages/navbar.min.html nav", () => {
    displayLoginSection(sessionStorage.getItem("isLoggedIn"));
  });
  $(".footer").load("./pages/footer.min.html footer");
  $(".login-overlay").load("./pages/modal.min.html .modal-container");

  /* Navbar */
  openAndCloseMenu();
  showSearchBar();
  onResizeWindow();

  /* Login/Register Modal */
  toggleLoginModal();
  submitLoginForm();
  submitRegisterForm();
  changeModalTab();
  logOut();

  /*Search*/
  searchFunc();

  /* Data fill*/
  //Extract Func to get passed in parameters
  function extractGame(params) {
    let paramsArr = params.split("=");
    return paramsArr[1];
  }
  //Get Data to Fill on Page based on parameters
  let dataonPage = data.filter((game) => {
    return game.id == extractGame(window.location.search);
  })[0];

  const dataFill = (data) => {
    const {
      name,
      genre,
      rating,
      trailerURL,
      shortDesc,
      headerImg,
      About,
      SysReq,
    } = data;

    document.title = `${name}`;

    $("#video #mainsrc").attr("src", `${trailerURL.localSource}`);
    $("#video #fallback").attr("src", `${trailerURL.fallBackSource}`);
    // Load the video again after attach src otherwise wont work.
    $("#video").get(0).load();

    $(".game-header-card img").attr("src", `${headerImg}`);

    $(".game-header-card h1.title").text(`${name}`);
    $("span.num-users-rating").text(`${rating.numRating}`);
    $("p.short-desc").text(`${shortDesc}`);
    $(".game-header-card span.category").text(`${genre}`);

    $("#devs").text(`${About.devs}`);
    $("#pubs").text(`${About.publishers}`);
    $("#released-date").text(`${About.releasedDate}`);
    $("#lang").text(`${About.lang}`);

    $("article.desc p").html(`${About.longDesc}`);

    [...$("article.screenshot img")].forEach((img, index) => {
      img.src = About.screenShot[index];
      img.alt = `${name} screenshot`;
    });

    [...$(".minimum p")].forEach((p) => {
      let spanText = p.getAttribute("id");
      p.innerHTML = `<span>${spanText}: </span>${SysReq.PC.min[spanText]}`;
    });

    [...$(".recommended p")].forEach((p) => {
      let spanText = p.getAttribute("id");
      p.innerHTML = `<span>${spanText}: </span>${SysReq.PC.rec[spanText]}`;
    });

    $("span.number-rating").text(`${rating.score}/5`);
    $("span.numbers-user-rated").text(`${rating.numRating} reviews`);
  };

  dataFill(dataonPage);

  /* Click event on screenshot */
  $(".screenshot-wrapper img").on("click", function () {
    //Check if we are already in overlay screenshot mode
    if ($("body").children(".overlay").length) {
      return;
    }
    //If not in overlay, create one and append to body.
    const overlay = $("<div></div>");
    overlay.addClass("overlay");
    $("body").append(overlay);
    //Prevent scrolling when in overlay mode
    $("body").css("overflow", "hidden");
    //Create new screenshot with larger width and height for clicked screenshot and append it to overlay.
    const selectedScreenshot = $("<img />");
    selectedScreenshot.css({ width: "calc(525px*2.5)", height: "auto" });
    selectedScreenshot.attr({
      src: `${$(this).attr("src")}`,
      alt: `${$(this).attr("alt")}`,
    });
    overlay.append(selectedScreenshot);

    //Get out of overlay mode when clicked on overlay.
    $(".overlay").on("click", (e) => {
      //Not firing when click on img
      if (e.target !== e.currentTarget) {
        return;
      }
      $("body .overlay").remove();
      $("body").css("overflow", "unset");
    });
  });

  /* Card Component */
  $(".submitAnchorTag").on("click", function () {
    $(this).closest("form").submit();
  });

  [...$(".category")].forEach((ele) => {
    ele.classList.add(`${ele.innerText}`);
  });
});
